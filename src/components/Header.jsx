import React from 'react'

const Header = () => {
  return (
    <div>
        <h1>Header</h1>
        <h3>{import.meta.env.VITE_API_URL}</h3>
    </div>
  )
}

export default Header